// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine', '@angular-devkit/build-angular'],
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),

            require('karma-phantomjs-launcher'),
            require('karma-coverage-istanbul-reporter'),
            require('@angular-devkit/build-angular/plugins/karma'),
            require('karma-junit-reporter')
        ],
        client: {
            clearContext: false // leave Jasmine Spec Runner output visible in browser...
        },
        coverageIstanbulReporter: {
            dir: require('path').join(__dirname, '../coverage/app2'),
            reports: ['html', 'lcovonly', 'text-summary'],
            fixWebpackSourcePaths: true
        },
        reporters: ['progress', 'kjhtml', 'dots', 'junit'],
        junitReporter: {
            outputFile: 'test-results.xml'
        },
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['PhantomJS'],
        customLaunchers: {
            Chrome_without_sandbox: {
                base: 'PhantomJS',
                flags: ['--no-sandbox'] // with sandbox it fails under Docker
            }
        },

        singleRun: true,
        restartOnFileChange: true
    });
};